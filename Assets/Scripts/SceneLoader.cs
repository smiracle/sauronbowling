﻿using UnityEngine;
using System.Collections;

public class SceneLoader : MonoBehaviour
{
    void Start()
    {
    }

    public void LoadLevel(string name)
    {
        Application.LoadLevel(name);
    }

    public void QuitRequest()
    {
        Application.Quit();
    }

    public void LoadNextLevel()
    {
        Application.LoadLevel(Application.loadedLevel + 1);
    }
}
