﻿using UnityEngine;

public class Shredder : MonoBehaviour
{
    public GameManager GameManager;
    void OnTriggerExit(Collider collider)
    {
        GameObject exitingObject = collider.gameObject;

        if (exitingObject.GetComponent<Pin>())
        {
            Destroy(exitingObject, 0.1f);
            exitingObject.SetActive(false);
        }
    }
}
