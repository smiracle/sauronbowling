﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text endGameTextMessage;
    public Text endGameScoreText;
    public GameObject endGamePanel;
    public GameObject InstructionsPanel;

    public BowlingScoreHelper BowlingScoreHelper { get; private set; }
    [HideInInspector] public bool IsEndGame = false;
    private PinSetter pinSetter;
    private ScoreDisplay scoreDisplay;
    private Ball ball;

    private void Awake()
    {
        BowlingScoreHelper = new BowlingScoreHelper();

        //Use this to test frames 
        //BowlingScoreHelper.AddRoll(10 - BowlingScoreHelper.PrevNumFallenPins - 3);
    }

    void Start()
    {
        scoreDisplay = FindObjectOfType<ScoreDisplay>();
        pinSetter = FindObjectOfType<PinSetter>();
        ball = FindObjectOfType<Ball>();
    }

    public Pin[] GetActivePins()
    {
        // This is horribly inefficient, never do this in a commercial game.
        return FindObjectsOfType<Pin>().Where(x => x.gameObject.activeInHierarchy).ToArray();
    }

    public void AdvanceFrame()
    {
        InstructionsPanel.SetActive(false);
        if (BowlingScoreHelper.Rolls.Count < 18)
        {
            BowlingScoreHelper.AddRoll(10 - BowlingScoreHelper.PrevNumFallenPins - GetNumPinsStanding());
        }
        else
        {
            BowlingScoreHelper.AddRoll(10 - GetNumPinsStanding());
        }
        
        ball.Reset();
        pinSetter.PerformAction(ActionMaster.NextAction(BowlingScoreHelper.Rolls));

        scoreDisplay.FillRolls();
        scoreDisplay.FillFrames();
        if (IsEndGame)
        {
            ShowEndGamePanel();
        }
    }

    public int GetNumPinsStanding()
    {
        var standing = GetActivePins().Where(x => x.IsStanding()).ToArray().Length;
        return standing;
    }

    public void ShowEndGamePanel()
    {
        endGamePanel.SetActive(true);
        int finalScore = BowlingScoreHelper.Frames.Sum();
        endGameScoreText.text = finalScore.ToString();
        if (finalScore < 50)
        {
            endGameTextMessage.text = "AND IS GREATLY DISPLEASED";
        }
        else if (finalScore < 100)
        {
            endGameTextMessage.text = "AND IS DISPLEASED";
        }
        else if (finalScore < 150)
        {
            endGameTextMessage.text = "AND IS GREATLY PLEASED";
        }
        else
        {
            endGameTextMessage.text = "AND IS ASTONISHED";
        }
    }
}