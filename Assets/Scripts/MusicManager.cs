﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour
{
    public AudioClip[] levelMusicChangeArray;
    private AudioSource audioSource;
    private static MusicManager instance = null;
    public static MusicManager Instance
    {
        get { return instance; }
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            audioSource = GetComponent<AudioSource>();
        }

        DontDestroyOnLoad(audioSource);
    }

    void Start()
    {
        if (audioSource.isPlaying == false)
        {
            PlayMusic();
        }
    }

    void PlayMusic()
    {
        audioSource.clip = levelMusicChangeArray[0];
        audioSource.loop = true;
        audioSource.Play();
    }

    public void SetVolume(float volume)
    {
        audioSource.volume = volume;
    }
}
