﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PinCounter : MonoBehaviour
{
    public Text standingDisplay;
    public Text velocityDisplay;
    public GameManager GameManager;
    private PinSetter pinSetter;
    private int numStandingPins = 0;
    private float settleTime = 3f; //How long to wait to consider pins settled in seconds
    private float ballSpeedThreshold = 1f;
    private float lastChangeTime;
    private int changeCounter = 0;
    private Ball ball;

    private float highestVelocity;
    private float ballSpeed;

    void Start()
    {
        ball = FindObjectOfType<Ball>();
        pinSetter = FindObjectOfType<PinSetter>();
        standingDisplay.color = Color.green;
    }

    void Update()
    {
        ballSpeed = ball.GetBallSpeed();
        highestVelocity = (ballSpeed > highestVelocity && ball.IsOutOfBounds == false) ? ballSpeed : highestVelocity;

        velocityDisplay.text = ball.GetBallSpeed().ToString();
        
        if (ball.IsOutOfBounds || (ball.IsInPlay && (!ball.IsMoving() || (ball.GetBallSpeed() < ballSpeedThreshold))))
        {
            // The ball is either out of bounds or in play but moving extremely slowly
            int currentNumPinsStanding = GameManager.GetNumPinsStanding();

            if (currentNumPinsStanding != numStandingPins)
            {
                lastChangeTime = Time.time;
                changeCounter++;
                numStandingPins = currentNumPinsStanding;
                standingDisplay.color = Color.red;
                return;
            }
            else
            {
                standingDisplay.color = Color.green;
            }

            if ((Time.time - lastChangeTime) > settleTime || changeCounter > 20f)
            {
                changeCounter = 0;
                GameManager.AdvanceFrame();
            }
        }

        if (!pinSetter.isBusy)
        {
            standingDisplay.text = GameManager.GetNumPinsStanding().ToString();
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name == "Ball")
        {
            ball.IsOutOfBounds = true;
            lastChangeTime = Time.time;
            highestVelocity = 0;
        }
    }

}
