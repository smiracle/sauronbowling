﻿using System.Collections.Generic;
using System.Text;

public class BowlingScoreHelper
{
    public int PrevNumFallenPins { get; private set; }
    public List<int> Rolls { get; private set; } = new List<int>();
    public List<int> Frames { get; private set; } = new List<int>();
    private bool spareInPreviousFrame;
    private int strikeCharges;
    private bool allowFrame10BonusRoll;

    public void AddRoll(int numFallenPins)
    {
        Rolls.Add(numFallenPins);
        if (Rolls.Count > 18)
        {
            switch (Rolls.Count)
            {
                case 19:
                    HandleFrame10Roll1(numFallenPins);
                    break;
                case 20:
                    HandleFrame10Roll2(numFallenPins);
                    break;
                case 21:
                    // 3rd roll of frame 10
                    HandleFrame10Roll3(numFallenPins);
                    if (!allowFrame10BonusRoll)
                    {
                        throw new System.Exception("3rd roll in frame 10 is not allowed since neither a strike was rolled first, nor a spare rolled 2nd in frame 10");
                    }
                    break;
            }
        }
        else if (Rolls.Count % 2 == 0)
        {
            // 2nd roll of the frame
            if (PrevNumFallenPins + numFallenPins == 10)
            {
                // Spare
                spareInPreviousFrame = true;
            }
            if (strikeCharges > 0)
            {
                // Apply strike bonus
                strikeCharges--;
                Frames[Frames.Count - 1] += numFallenPins;
            }

            Frames.Add(PrevNumFallenPins + numFallenPins);
            PrevNumFallenPins = 0;
        }
        else
        {
            // 1st roll of the frame
            if (spareInPreviousFrame)
            {
                // Apply spare bonus
                spareInPreviousFrame = false;
                Frames[Frames.Count - 1] += numFallenPins;
            }

            if (strikeCharges > 0)
            {
                // Apply strike bonus
                strikeCharges--;
                Frames[Frames.Count - 1] += numFallenPins;

                // Check for two strikes in a row
                if (Rolls.Count >= 5
                    && Rolls[Rolls.Count - 2] == 0 && Rolls[Rolls.Count - 3] == 10
                    && Rolls[Rolls.Count - 4] == 0 && Rolls[Rolls.Count - 5] == 10)
                {
                    Frames[Frames.Count - 2] += numFallenPins;
                }
            }

            if (numFallenPins == 10)
            {
                // Strike
                PrevNumFallenPins = 0;
                Rolls.Add(0);
                Frames.Add(10);
                strikeCharges = 2;
            }
            else
            {
                // Open frame scoring
                PrevNumFallenPins = numFallenPins;
            }
        }
    }

    private void HandleFrame10Roll1(int numFallenPins)
    {
        // 1st roll of frame 10
        // Always add the score for this frame in immediately
        Frames.Add(numFallenPins);
        if (spareInPreviousFrame)
        {
            // Apply spare bonus
            spareInPreviousFrame = false;
            Frames[Frames.Count - 2] += numFallenPins;
        }

        if (strikeCharges > 0)
        {
            // Apply strike bonus
            strikeCharges--;
            Frames[Frames.Count - 2] += numFallenPins;

            // Check for two strikes in a row
            if (Rolls[Rolls.Count - 2] == 0 && Rolls[Rolls.Count - 3] == 10
                && Rolls[Rolls.Count - 4] == 0 && Rolls[Rolls.Count - 5] == 10)
            {
                Frames[Frames.Count - 3] += numFallenPins;
            }
        }

        if (numFallenPins == 10)
        {
            // Strike
            allowFrame10BonusRoll = true;
            strikeCharges = 2;
        }
        // Open frame scoring
        PrevNumFallenPins = numFallenPins;
    }

    private void HandleFrame10Roll2(int numFallenPins)
    {
        if (Rolls[18] + numFallenPins == 10 && Rolls[18] != 10)
        {
            // Spare
            allowFrame10BonusRoll = true;
        }
        if (strikeCharges > 0)
        {
            // Apply strike bonus
            Frames[Frames.Count - 2] += numFallenPins;
        }
        Frames[Frames.Count - 1] += numFallenPins;
    }

    private void HandleFrame10Roll3(int numFallenPins)
    {
        Frames[Frames.Count - 1] += numFallenPins;
    }

    public string GetRollsAsFormattedString()
    {
        var sb = new StringBuilder();
        for (int i = 0; i < Rolls.Count && i < 18; i++)
        {
            if (i % 2 == 0 && Rolls[i] == 10)
            {
                sb.Append("X ");
                i++;
            }
            else if (i % 2 != 0 && Rolls[i] + Rolls[i - 1] == 10)
            {
                sb.Append("/");
            }
            else
            {
                // Open frame scoring (0-9)
                sb.Append(Rolls[i]);
            }
        }
        for (int j = 18; j < Rolls.Count; j++)
        {
            if (Rolls[j] == 10)
            {
                sb.Append("X");
            }
            else if (j > 18 && Rolls[j] + Rolls[j - 1] == 10)
            {
                sb.Append("/");
            }
            else
            {
                sb.Append(Rolls[j]);
            }
        }
        return sb.ToString();
    }
}