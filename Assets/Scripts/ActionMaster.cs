﻿using System.Collections.Generic;

public static class ActionMaster
{
    public static Actions NextAction(List<int> rolls)
    {
        Actions nextAction = Actions.Undefined;

        for (int i = 0; i < rolls.Count; i++)
        { // Step through rolls

            if (i == 20)
            {
                nextAction = Actions.EndGame;
            }
            else if (i >= 18 && rolls[i] == 10)
            { // Handle last-frame special cases
                nextAction = Actions.Reset;
            }
            else if (i == 19)
            {
                if (rolls[18] == 10 && rolls[19] == 0)
                {
                    nextAction = Actions.Cleanup;
                }
                else if (rolls[18] + rolls[19] == 10)
                {
                    nextAction = Actions.Reset;
                }
                else if (rolls[18] + rolls[19] >= 10)
                {  // Roll 21 awarded
                    nextAction = Actions.Cleanup;
                }
                else
                {
                    nextAction = Actions.EndGame;
                }
            }
            else if (i % 2 == 0)
            {
                // First bowl of frame
                if (rolls[i] == 10)
                {
                    nextAction = Actions.EndTurn;
                }
                else
                {
                    nextAction = Actions.Cleanup;
                }
            }
            else
            {
                // Second bowl of frame
                nextAction = Actions.EndTurn;
            }
        }

        return nextAction;
    }
}