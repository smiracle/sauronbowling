﻿using UnityEngine;

public class Pin : MonoBehaviour
{
    private float standingThreshold = 7f;
    private float speedThreshold = 5f;
    private float distToRaise = 40f;
    private Rigidbody rigidBody;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    public bool IsStanding()
    {
        Vector3 rotationInEuler = transform.rotation.eulerAngles;
        float tiltInX = Mathf.Abs(270 - rotationInEuler.x);
        if (tiltInX < standingThreshold && rigidBody.velocity.magnitude < speedThreshold)
        {
            return true;
        }

        return false;
    }

    public void RaisePinIfStanding()
    {
        if (IsStanding())
        {
            rigidBody.useGravity = false;
            transform.Translate(new Vector3(0, distToRaise, 0), Space.World);
            transform.rotation = Quaternion.Euler(270f, 0, 0);
        }
    }

    public void LowerPin()
    {
        transform.Translate(new Vector3(0, -distToRaise, 0), Space.World);
        rigidBody.useGravity = false;
    }
}