﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    public bool IsInPlay = false;
    public bool IsOutOfBounds = false;
    private Vector3 ballStartPos;
    private Rigidbody rigidBody;
    private AudioSource audioSource1;
    private AudioSource audioSource2;
    
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.useGravity = false;
        ballStartPos = transform.position;
        AudioSource[] aSources = GetComponents<AudioSource>();
        audioSource1 = aSources[0];
        audioSource2 = aSources[1];
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name.StartsWith("Bowling Pin"))
        {
            audioSource2.Play();
        }
    }

    public bool IsMoving()
    {
        return rigidBody.velocity.magnitude > 0f;
    }

    public float GetBallSpeed()
    {
        float speed = rigidBody.velocity.magnitude;
        if (speed != 0)
        {
            speed /= 10;
        }
        return Mathf.Round(speed);
    }

    public void Launch(Vector3 velocity)
    {
        IsInPlay = true;
        rigidBody.useGravity = true;
        if (velocity.magnitude > 0)
        {
            rigidBody.velocity = velocity;
        }
        audioSource1.Play();
    }

    public void Reset()
    {
        IsInPlay = false;
        IsOutOfBounds = false;
        transform.position = ballStartPos;
        transform.rotation = Quaternion.identity;
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = Vector3.zero;
        rigidBody.useGravity = false;
    }
}