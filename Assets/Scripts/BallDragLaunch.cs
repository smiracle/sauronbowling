﻿using UnityEngine;

[RequireComponent(typeof(Ball))]
public class BallDragLaunch : MonoBehaviour
{
    private Vector3 dragStart, dragEnd;
    private float startTime, endTime;
    private Ball ball;
    private GameManager gameManager;

    void Start()
    {
        ball = GetComponent<Ball>();
        gameManager = FindObjectOfType<GameManager>();
    }

    public void MoveStart(float amount)
    {
        if (!ball.IsInPlay)
        {
            float xPos = Mathf.Clamp(ball.transform.position.x + amount, -50f, 50f);
            float yPos = ball.transform.position.y;
            float zPos = ball.transform.position.z;
            ball.transform.position = new Vector3(xPos, yPos, zPos);
        }
    }

    public void DragStart()
    {
        if (!ball.IsInPlay && !gameManager.IsEndGame)
        {
            // Capture time & position of drag start
            dragStart = Input.mousePosition;
            startTime = Time.time;
        }
    }

    public void DragEnd()
    {
        if (!ball.IsInPlay && !gameManager.IsEndGame)
        {
            // Launch the ball
            dragEnd = Input.mousePosition;
            endTime = Time.time;

            float dragDuration = endTime - startTime;
            float launchSpeedX = (dragEnd.x - dragStart.x);
            float launchSpeedZ = (dragEnd.y - dragStart.y);

            launchSpeedZ *= 3.0f;

            Mathf.Clamp(launchSpeedZ, -1500f, 1500f);
            Mathf.Clamp(launchSpeedX, -1500f, 1500f);

            //Debug.Log(launchSpeedZ);
            //Debug.Log(launchSpeedX);

            Vector3 launchVelocity = new Vector3(launchSpeedX, 0, launchSpeedZ);
            ball.Launch(launchVelocity);
        }
    }
}
