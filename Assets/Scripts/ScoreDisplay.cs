﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
    public Text[] rollTexts, frameTexts;
    private GameManager gameManager;
    private ScoreDisplay scoreDisplay;

    private void Awake()
    {
        gameManager = FindObjectOfType<GameManager>();
        scoreDisplay = FindObjectOfType<ScoreDisplay>();
    }

    private void Start()
    {
        scoreDisplay.FillRolls();
        scoreDisplay.FillFrames();
    }

    public void FillRolls()
    {
        string scoresString = gameManager.BowlingScoreHelper.GetRollsAsFormattedString();
        for (int i = 0; i < scoresString.Length; i++)
        {
            if (i < scoresString.Length && i < rollTexts.Length)
            {
                rollTexts[i].text = scoresString[i].ToString();
            }
        }
    }

    public void FillFrames()
    {
        var runningTotal = 0;
        for (int i = 0; i < gameManager.BowlingScoreHelper.Frames.Count; i++)
        {
            runningTotal += gameManager.BowlingScoreHelper.Frames[i];
            frameTexts[i].text = runningTotal.ToString();
        }
    }
}