﻿using System.Collections;
using UnityEngine;

public class PinSetter : MonoBehaviour
{
    public GameManager GameManager;
    public GameObject pinSet;
    public Animator Animator;
    private AnimationClip animationSwipe;
    public bool isBusy { get; private set; }

    void Start()
    {
        animationSwipe = GetAnimationClip("Swipe");
    }

    public AnimationClip GetAnimationClip(string name)
    {
        foreach (AnimationClip clip in Animator.runtimeAnimatorController.animationClips)
        {
            if (clip.name == name)
            {
                return clip;
            }
        }
        return null; // no clip by that name
    }

    public void RaisePins()
    {
        foreach (Pin pin in GameManager.GetActivePins())
        {
            pin.RaisePinIfStanding();
        }
    }

    public void LowerPins()
    {
        foreach (Pin pin in GameManager.GetActivePins())
        {
            pin.LowerPin();
        }
    }

    public void RenewPins()
    {
        // Delete the parent pins object
        var pinParents = GameObject.FindGameObjectsWithTag("Pins");
        foreach(var pinParent in pinParents)
        {
            Destroy(pinParent);
        }
        Instantiate(pinSet, new Vector3(0, 0, 1829), Quaternion.identity); //quaternion.identity means no rotation
    }

    public void PerformAction(Actions action)
    {
        switch (action)
        {
            case Actions.Cleanup:
                RaisePins();
                Animator.SetTrigger("cleanupTrigger");
                LowerPins();
                StartCoroutine(WaitForSwipeAnimation());
                break;
            case Actions.EndTurn:
                Animator.SetTrigger("resetTrigger");
                StartCoroutine(WaitForSwipeAnimation());
                break;
            case Actions.Reset:
                Animator.SetTrigger("resetTrigger");
                StartCoroutine(WaitForSwipeAnimation());
                break;
            case Actions.EndGame:
                //Display Endgame UI
                GameManager.IsEndGame = true;
                break;
        }
    }

    IEnumerator WaitForSwipeAnimation()
    {
        isBusy = true;
        yield return new WaitForSeconds(animationSwipe.length);
        isBusy = false;
    }
}